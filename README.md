# ocaml-baremetal-riscv
---

This is repository which can produce riscv-bootable elf files from a given ocaml object file.

### Requirements

- installed instance of [shakti-tee-tools](https://gitlab.com/sl33k/shakti-tee-tools.git), available in `toolchain/` subfolder.
- installed instance of [riscv-qemu](https://github.com/riscv/riscv-qemu)

### Prepare Repository

    $ git submodule update --init --recursive

### <a name="quickstart"></a>Build a kernel

Generate an appropriate ocaml object file (see this [README](prebuilt/))
then:

    $ cp /path/to/object.o prebuilt/payload.o
	$ mkdir build && cd build 
    $ cmake ..
    $ make

The generated `rv64` ELF file is `kernel`.

### Run a kernel

    $ qemu-system-riscv64 \
        -machine spike_v1.10 \
        -smp 1 \
        -m 1G \
        -serial stdio \
        -kernel kernel

### Known Issues

- changing/update `ocaml-freestanding` might require a removal of the build-folder contents due to the hacky compililation process of `ocaml-freestanding`.
